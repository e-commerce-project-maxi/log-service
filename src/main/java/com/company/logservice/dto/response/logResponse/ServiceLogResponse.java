package com.company.logservice.dto.response.logResponse;

import com.company.logservice.enums.ActionTypeEnum;
import com.company.logservice.enums.ServiceNameEnum;
import com.company.logservice.logModel.JsonDataModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ServiceLogResponse {
    Long id;
    Long executedUserId;
    String username;
    Long serviceId;
    ServiceNameEnum serviceName;
    ActionTypeEnum actionType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime logCreationTime;
    JsonDataModel item;
}

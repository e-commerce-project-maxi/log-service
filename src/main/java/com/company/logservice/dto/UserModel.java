package com.company.logservice.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserModel implements Serializable {
    private String firstName;
    private String lastName;
}

package com.company.logservice.dto.request;

import com.company.logservice.enums.ActionTypeEnum;
import com.company.logservice.enums.ServiceNameEnum;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ServiceLogRequest {
    Long userId;
    String username;
    Long serviceId;
    ServiceNameEnum serviceName;
    ActionTypeEnum actionType;
    LogModelRequest logModelRequest;
}

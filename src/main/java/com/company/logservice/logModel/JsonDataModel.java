package com.company.logservice.logModel;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JsonDataModel<NEW extends Serializable, OLD extends Serializable> implements Serializable {
    NEW newData;
    OLD oldData;
}

package com.company.logservice.entity;

import com.company.logservice.enums.ActionTypeEnum;
import com.company.logservice.enums.ServiceNameEnum;
import com.company.logservice.logModel.LogModel;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.company.logservice.constant.CommonConstants.EntityConstants.JSONB;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@TypeDef(name = JSONB, typeClass = JsonBinaryType.class)
public class ServiceLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Long executedUserId;
    String username;
    Long serviceId;
    ServiceNameEnum serviceName;
    ActionTypeEnum actionType;
    LocalDateTime logCreationTime;

    @Type(type = JSONB)
    @Column(columnDefinition = JSONB)
    LogModel logData;
}

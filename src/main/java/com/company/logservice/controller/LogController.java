package com.company.logservice.controller;

import com.company.logservice.dto.response.logResponse.ServiceLogResponse;
import com.company.logservice.enums.ServiceNameEnum;
import com.company.logservice.service.business.LogBusinessService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class LogController {

    private final LogBusinessService logBusinessService;

    @GetMapping
    public List<ServiceLogResponse> getLogs(@RequestParam ServiceNameEnum name) {
        return logBusinessService.getLogByServiceName(name);
    }
}

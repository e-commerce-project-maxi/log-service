package com.company.logservice.container;

public interface BaseException {

    String getCode();

    String getDescription();
}

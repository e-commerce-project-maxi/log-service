package com.company.logservice.container.api;

public class ApiConstants {
    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAILED = "OPERATION_FAILED";
}

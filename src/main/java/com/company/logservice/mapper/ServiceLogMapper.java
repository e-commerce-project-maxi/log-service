package com.company.logservice.mapper;

import com.company.logservice.dto.request.LogModelRequest;
import com.company.logservice.dto.request.ServiceLogRequest;
import com.company.logservice.entity.ServiceLog;
import com.company.logservice.logModel.JsonDataModel;
import com.company.logservice.logModel.LogModel;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ServiceLogMapper {

    public ServiceLog createServiceLog(ServiceLogRequest logRequest) {
        ServiceLog serviceLog = new ServiceLog();
        serviceLog.setExecutedUserId(logRequest.getUserId());
        serviceLog.setUsername(logRequest.getUsername());
        serviceLog.setServiceName(logRequest.getServiceName());
        serviceLog.setActionType(logRequest.getActionType());
        serviceLog.setServiceId(logRequest.getServiceId());
        serviceLog.setLogCreationTime(LocalDateTime.now());
        serviceLog.setLogData(createJsonModel(logRequest.getLogModelRequest()));
        return serviceLog;
    }

    private LogModel createJsonModel(LogModelRequest modelRequest) {
        JsonDataModel jsonDataModel = new JsonDataModel();
        jsonDataModel.setNewData(modelRequest.getNewData());
        jsonDataModel.setOldData(modelRequest.getOldData());
        return new LogModel(jsonDataModel);
    }
}

package com.company.logservice.mapper;

import com.company.logservice.dto.response.logResponse.ServiceLogResponse;
import com.company.logservice.entity.ServiceLog;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ServiceLogResponseMapper {

    public List<ServiceLogResponse> toResponseList(List<ServiceLog> serviceLogs) {
        return serviceLogs.stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }

    public ServiceLogResponse toResponse(ServiceLog serviceLog) {
        ServiceLogResponse response = new ServiceLogResponse();
        response.setId(serviceLog.getId());
        response.setUsername(serviceLog.getUsername());
        response.setServiceId(serviceLog.getServiceId());
        response.setLogCreationTime(serviceLog.getLogCreationTime());
        response.setExecutedUserId(serviceLog.getExecutedUserId());
        response.setServiceName(serviceLog.getServiceName());
        response.setActionType(serviceLog.getActionType());
        response.setItem(serviceLog.getLogData().getItem());
        return response;
    }
}

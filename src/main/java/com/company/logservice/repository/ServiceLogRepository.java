package com.company.logservice.repository;

import com.company.logservice.entity.ServiceLog;
import com.company.logservice.enums.ServiceNameEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceLogRepository extends JpaRepository<ServiceLog, Long> {

    List<ServiceLog> getServiceLogByServiceName(ServiceNameEnum serviceNameEnum);
}

package com.company.logservice.service.business;

import com.company.logservice.dto.request.ServiceLogRequest;
import com.company.logservice.dto.response.logResponse.ServiceLogResponse;
import com.company.logservice.enums.ServiceNameEnum;

import java.util.List;

public interface LogBusinessService {

    void saveLog(ServiceLogRequest serviceLogRequest);

    List<ServiceLogResponse> getLogByServiceName(ServiceNameEnum serviceNameEnum);
}

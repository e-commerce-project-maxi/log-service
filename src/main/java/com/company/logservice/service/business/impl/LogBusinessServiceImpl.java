package com.company.logservice.service.business.impl;

import com.company.logservice.dto.request.ServiceLogRequest;
import com.company.logservice.dto.response.logResponse.ServiceLogResponse;
import com.company.logservice.enums.ServiceNameEnum;
import com.company.logservice.mapper.ServiceLogMapper;
import com.company.logservice.mapper.ServiceLogResponseMapper;
import com.company.logservice.service.business.LogBusinessService;
import com.company.logservice.service.repoService.LogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LogBusinessServiceImpl implements LogBusinessService {

    private final ServiceLogMapper serviceLogMapper;
    private final ServiceLogResponseMapper serviceLogResponseMapper;
    private final LogService logService;

    @Override
    public void saveLog(ServiceLogRequest serviceLogRequest) {
        logService.saveLog(serviceLogMapper.createServiceLog(serviceLogRequest));
    }

    @Override
    public List<ServiceLogResponse> getLogByServiceName(ServiceNameEnum serviceNameEnum) {
        return serviceLogResponseMapper.toResponseList(logService.getLogByService(serviceNameEnum));
    }
}

package com.company.logservice.service.repoService.impl;

import com.company.logservice.entity.ServiceLog;
import com.company.logservice.enums.ServiceNameEnum;
import com.company.logservice.repository.ServiceLogRepository;
import com.company.logservice.service.repoService.LogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LogServiceImpl implements LogService {

    private final ServiceLogRepository serviceLogRepository;

    @Override
    public void saveLog(ServiceLog serviceLog) {
        serviceLogRepository.save(serviceLog);
    }

    @Override
    public List<ServiceLog> getLogByService(ServiceNameEnum serviceNameEnum) {
        return serviceLogRepository.getServiceLogByServiceName(serviceNameEnum);
    }
}

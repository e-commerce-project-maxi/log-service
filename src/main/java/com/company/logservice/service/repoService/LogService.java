package com.company.logservice.service.repoService;

import com.company.logservice.entity.ServiceLog;
import com.company.logservice.enums.ServiceNameEnum;

import java.util.List;

public interface LogService {

    void saveLog(ServiceLog serviceLog);

    List<ServiceLog> getLogByService(ServiceNameEnum serviceNameEnum);
}

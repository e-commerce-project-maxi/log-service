package com.company.logservice.enums;

public enum ServiceNameEnum {
    LOG_IN,
    LOG_OUT,
    FORGOT_PASSWORD,
    USER_OPERATION,
    USER_STATUS_OPERATION,
    USER_CHANGE_PASSWORD,
    USER_ROLE;
}

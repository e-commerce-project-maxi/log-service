package com.company.logservice.enums;

public enum ActionTypeEnum {
    CREATE(1), UPDATE(2);

    private int id;

    ActionTypeEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
